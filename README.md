[![version](https://lbsn.vgiscience.org/structure/hlldb/version.svg)](https://gitlab.vgiscience.de/lbsn/structure/hlldb/tree/master) [![pipeline status](https://gitlab.vgiscience.de/lbsn/structure/hlldb/badges/master/pipeline.svg)](https://gitlab.vgiscience.de/lbsn/structure/hlldb/commits/master)

# LBSN STRUCTURE HLL - SQL Files

The base SQL HLL structure of the [common location based social network (LBSN) data structure concept](https://lbsn.vgiscience.org) to handle cross network Social Media data.

This is the LBSN HLL version, which abstracts the raw version of the structure into a probabilistic hyper-log-log scheme, which can be used in privacy-aware visual analytics.

A complete guide is provided in the [LBSN documentation](https://lbsn.vgiscience.org)