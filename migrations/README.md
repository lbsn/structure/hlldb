# Migrations

The migrations provided here can be used as a base for   
migrating live servers that contain already data.  

These migrations are provided without warranty: Use with caution.  

Migrations created with [migra][migra], verified
and optionally modified manually.

Migra script example:

```bash
migra postgresql://postgres:{POSTGRES_PASSWORD}@127.0.0.1:25432/hlldb \
      postgresql://postgres:{POSTGRES_PASSWORD}@127.0.0.1:25433/hlldb \
      --unsafe \
      | tee migrations.sql
```

[migra]: https://databaseci.com/docs/migra