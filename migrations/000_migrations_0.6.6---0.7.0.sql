/*
 * Schema migrations for upgrading from 
 * LBSN Structure (hll) version 0.6.6 to 0.7.0
 *
 * BREAKING CHANGE: All "date_hll" columns will be DROPPED.
 *
 * Notes:
 * - Generated with [migra][migra]
 * - CAP style: Manual overrides of migra sql
 *
 * migra: https://databaseci.com/docs/migra
 */
 
CREATE TABLE temporal.pud_hll (
  pud_hll hll
);

COMMENT ON COLUMN temporal. "pud_hll".pud_hll IS 'Temporal Facet
 across Social Facet - User Post Days (PUD) Count,
 typical measurement for counting distinct user posts per day ("userdays"), 
 as coined by Wood, Guerry, Silver and Lacayo (2013),
 referenced by origin_id, user_guid, and date (yyyy-mm-dd)
 Format: origin_id:user_guid:date';

ALTER TABLE "spatial"."latlng"
NO INHERIT temporal.date_hll;

ALTER TABLE "spatial"."latlng"
DROP COLUMN "date_hll";

ALTER TABLE "spatial"."latlng"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "spatial"."latlng"
INHERIT temporal.pud_hll;

ALTER TABLE "spatial"."place"
NO INHERIT temporal.date_hll;

ALTER TABLE "spatial"."place"
DROP COLUMN "date_hll";

ALTER TABLE "spatial"."place"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "spatial"."place"
INHERIT temporal.pud_hll;

ALTER TABLE "spatial"."city"
NO INHERIT temporal.date_hll;

ALTER TABLE "spatial"."city"
DROP COLUMN "date_hll";

ALTER TABLE "spatial"."city"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "spatial"."city"
INHERIT temporal.pud_hll;

ALTER TABLE "spatial"."region"
NO INHERIT temporal.date_hll;

ALTER TABLE "spatial"."region"
DROP COLUMN "date_hll";

ALTER TABLE "spatial"."region"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "spatial"."region"
INHERIT temporal.pud_hll;

ALTER TABLE "spatial"."country"
NO INHERIT temporal.date_hll;

ALTER TABLE "spatial"."country"
DROP COLUMN "date_hll";

ALTER TABLE "spatial"."country"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "spatial"."country"
INHERIT temporal.pud_hll;

ALTER TABLE "social"."friends"
NO INHERIT temporal.date_hll;

ALTER TABLE "social"."friends"
DROP COLUMN "date_hll";

ALTER TABLE "social"."friends"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "social"."friends"
INHERIT temporal.pud_hll;

ALTER TABLE "social"."community"
NO INHERIT temporal.date_hll;

ALTER TABLE "social"."community"
DROP COLUMN "date_hll";

ALTER TABLE "social"."community"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "social"."community"
INHERIT temporal.pud_hll;

ALTER TABLE "social"."culture"
NO INHERIT temporal.date_hll;

ALTER TABLE "social"."culture"
DROP COLUMN "date_hll";

ALTER TABLE "social"."culture"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "social"."culture"
INHERIT temporal.pud_hll;

ALTER TABLE "topical"."hashtag"
NO INHERIT temporal.date_hll;

ALTER TABLE "topical"."hashtag"
DROP COLUMN "date_hll";

ALTER TABLE "topical"."hashtag"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "topical"."hashtag"
INHERIT temporal.pud_hll;

ALTER TABLE "topical"."emoji"
NO INHERIT temporal.date_hll;

ALTER TABLE "topical"."emoji"
DROP COLUMN "date_hll";

ALTER TABLE "topical"."emoji"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "topical"."emoji"
INHERIT temporal.pud_hll;

ALTER TABLE "topical"."term"
NO INHERIT temporal.date_hll;

ALTER TABLE "topical"."term"
DROP COLUMN "date_hll";

ALTER TABLE "topical"."term"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "topical"."term"
INHERIT temporal.pud_hll;

ALTER TABLE "topical"."topic"
NO INHERIT temporal.date_hll;

ALTER TABLE "topical"."topic"
DROP COLUMN "date_hll";

ALTER TABLE "topical"."topic"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "topical"."topic"
INHERIT temporal.pud_hll;

ALTER TABLE "topical"."domain"
NO INHERIT temporal.date_hll;

ALTER TABLE "topical"."domain"
DROP COLUMN "date_hll";

ALTER TABLE "topical"."domain"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "topical"."domain"
INHERIT temporal.pud_hll;

ALTER TABLE "topical"."_term_latlng"
NO INHERIT temporal.date_hll;

ALTER TABLE "topical"."_term_latlng"
DROP COLUMN "date_hll";

ALTER TABLE "topical"."_term_latlng"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "topical"."_term_latlng"
INHERIT temporal.pud_hll;

ALTER TABLE "topical"."_hashtag_latlng"
NO INHERIT temporal.date_hll;

ALTER TABLE "topical"."_hashtag_latlng"
DROP COLUMN "date_hll";

ALTER TABLE "topical"."_hashtag_latlng"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "topical"."_hashtag_latlng"
INHERIT temporal.pud_hll;

ALTER TABLE "topical"."_emoji_latlng"
NO INHERIT temporal.date_hll;

ALTER TABLE "topical"."_emoji_latlng"
DROP COLUMN "date_hll";

ALTER TABLE "topical"."_emoji_latlng"
ADD COLUMN "pud_hll" hll;

ALTER TABLE "topical"."_emoji_latlng"
INHERIT temporal.pud_hll;

CREATE TABLE temporal._month_latlng (
  year smallint,
  month smallint,
  latitude float,
  longitude float,
  PRIMARY KEY (year, month, latitude, longitude),
  latlng_geom geometry(Point, 4326) NOT NULL)
INHERITS (
  social.user_hll, -- e.g. number of users using this c-base
  topical.post_hll, -- e.g. number of posts with this c-base
  temporal.pud_hll -- e.g. number of distinct dates where this c-base is used
);

CREATE TABLE temporal._month_hashtag (
  month smallint,
  year smallint,
  hashtag text,
  PRIMARY KEY (year, month, hashtag))
INHERITS (
  social.user_hll, -- e.g. number of users using this c-base
  topical.post_hll, -- e.g. number of posts with this c-base
  temporal.pud_hll -- e.g. number of distinct dates where this c-base is used
);

CREATE OR REPLACE FUNCTION 
    extensions.hll_intersection (hll_a hll, hll_b hll)
RETURNS int
AS $$
    SELECT "is".intersection_count FROM
        (SELECT 
            hll_cardinality(hll_a)::int + 
            hll_cardinality(hll_b)::int - 
            hll_cardinality(hll_union(hll_a,hll_b))::int as intersection_count) as "is"
$$
LANGUAGE SQL
STRICT;


