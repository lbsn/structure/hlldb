/** HLL Measures and Base Structure
 * these SQL instructions are examples of typical HLL-Aggregate Schemes,
 * e.g. for visual analytics
 * they're not complete and should be considered as a demonstration structure,
 * remove or add based based on personal needs and application context.
 *
 * See publications:
 * [1] Löchner, M., Dunkel, A., & Burghardt, D. (2018).
 * A privacy-aware model to process data from location-based social media.
 * Konstanz 2018
 * [2] Protecting privacy using HyperLogLog to process data from Location
 * Based Social Networks", Authors: Marc Loechner, Alexander Dunkel,
 * Dirk Burghardt, Practical Implementation with Privacy, Ethics and Legal
 * Considerations, LESSON 2019 Zurich, Switzerland
 *
 * ----------------
 * Structure:
 * 1. Simple (Primary) HLL Metrics
 * 2. Composite (Secondary) HLL Metrics
 * 3. HLL Bases
 * 4. HLL Composite Bases
 *
 * ----------------
 * Starting below:
 * ----------------
 * 1. Simple (Primary) HLL Metrics
 * ----------------
 * Simple HLL Metrics consist of one-key sets
 * The simple metrics below are typical,
 * repeatedly used structures for hll_aggregation
 * we've provided one typical example measure
 * for each LBSN facet dimension
 */
CREATE TABLE social.user_hll (
  user_hll hll
);

COMMENT ON COLUMN social. "user_hll".user_hll IS 'Social Facet - User Count:
 smallest measurement for a social entity is a single user,
 which can be referenced by its origin_id and user_guid
 user_hll allows counting number of distinct users
 Format: origin_id:user_guid, e.g.:
 2:94355fac44ba1ca68484c72698f9d0d6';

CREATE TABLE topical.post_hll (
  post_hll hll
);

COMMENT ON COLUMN topical. "post_hll".post_hll IS 'Topical Facet - Post Count:
 typical measurement for a topical entity is a single post,
 which can be referenced by its origin_id and post_guid
 post_hll allows counting number of distinct posts
 Format: origin_id:post_guid, e.g.:
 1:60b59e3d330f31014ba4e905672a7f62d2893cedec3fb5e51a7fbe6f559e9f65';

CREATE TABLE topical.term_hll (
  term_hll hll
);

COMMENT ON COLUMN topical. "term_hll".term_hll IS 'Topical Facet - Term Count:
 smallest measurement for a topical entity is a single term,
 term_hll allows counting number of distinct terms used
 Format: term, e.g.: tree';

CREATE TABLE spatial.latlng_hll (
  latlng_hll hll
);

COMMENT ON COLUMN spatial. "latlng_hll".latlng_hll IS 'Spatial Facet -
 Location Count: smallest measurement for a spatial entity is a
 single location. which can be referenced by its lat and lng coordinate
 latlng_hll allows counting number of distinct locations
 Format: lat:lng, e.g.: 50.979603118469:5.9139262826066';

CREATE TABLE spatial.place_hll (
  place_hll hll
);

COMMENT ON COLUMN spatial. "place_hll".place_hll IS 'Spatial Facet -
 Place Count: typical measurement for a spatial entity is a
 single place, which can be referenced by its place_guid or geom_area
 place_hll allows counting number of distinct places
 Format: origin_id:place_guid, e.g.: 1:32f328e066';

CREATE TABLE temporal.date_hll (
  date_hll hll
);

COMMENT ON COLUMN temporal. "date_hll".date_hll IS 'Temporal Facet -
 Date Count: typical measurement for the temporal facet is a single
 (unique) day, which can be referenced by its year, month, and day
 date_hll allows counting number of distinct days
 Format: yyyy-mm-dd, e.g.: 2019-12-01';


/** 2. Composite (Secondary) HLL Metrics
 * ----------------
 * Composite HLL metrics span multiple facet dimensions
 * which are therefore more expensive to store and calculate,
 * but provide for more detailed analysis options
 * for specific contexts
 */
/**
 */
CREATE TABLE spatial.upl_hll (
  upl_hll hll
);

COMMENT ON COLUMN spatial. "upl_hll".upl_hll IS 'Social Facet
 across Spatial Facet - User Post Location (UPL) Count:
 typical measurement for counting distinct users across locations
 referenced by origin_id, user_guid, lat, lng
 upl_hll allows counting number of distinct user locations
 Format: origin_id:user_guid:lat:lng';

CREATE TABLE topical.utl_hll (
  utl_hll hll
);

COMMENT ON COLUMN topical. "utl_hll".utl_hll IS 'Social Facet
 across Topical and Spatial Facet - User Term Location (UTL) Count
 typical measurement for counting topical spread across locations
 referenced by origin_id, user_guid, term, lat, lng
 utl_hll allows counting number of distinct term locations
 Format: origin_id:user_guid:term:lat:lng';

CREATE TABLE topical.upt_hll (
  upt_hll hll
);

COMMENT ON COLUMN topical. "upt_hll".upt_hll IS 'Social Facet
 across Topical Facet - User Post Term (UPT) Count
 typical measurement for counting topical repetition
 referenced by origin_id, user_guid, term
 utl_hll allows counting number of distinct term per user
 Format: origin_id:user_guid:term';

CREATE TABLE temporal.pud_hll (
  pud_hll hll
);

COMMENT ON COLUMN temporal. "pud_hll".pud_hll IS 'Temporal Facet
 across Social Facet - User Post Days (PUD) Count,
 typical measurement for counting distinct user posts per day ("userdays"), 
 as coined by Wood, Guerry, Silver and Lacayo (2013),
 referenced by origin_id, user_guid, and date (yyyy-mm-dd)
 Format: origin_id:user_guid:date';
 
/** 3. HLL Bases
 * ----------------
 * Aggregation bases form the partitions based on which
 * hll metrics are measured
 * - each base must be unique and is therefore considered as the primary key
 * - some bases are composite keys, e.g. lat and lng
 * - each base can have additional columns for non-personal
 * descriptive data (e.g. a name for a location)
 * - for each base, a combination of hll metrics can be chosen
 *   using inheritance
 *
 * Aggregation Base Structure may be created for different
 * layers of granularity, see [1]
 * ----------------
 * Starting below:
 * ----------------
 * Spatial Facet Examples
 * ----------------
 * Coordinates
 * The Primary Key to identify coordinates is the lat/lng combination.
 */
CREATE TABLE spatial.latlng (
  latitude float,
  longitude float,
  PRIMARY KEY (latitude, longitude),
  latlng_geom geometry(Point, 4326) NOT NULL)
INHERITS (
  social.user_hll, -- e.g. number of users/latlng (=upl)
  topical.post_hll, -- e.g. number of posts/latlng
  temporal.pud_hll, -- e.g. number of userdays/latlng
  topical.utl_hll -- e.g. number of terms/latlng
);


/** Places
 * For places and any other named entities,
 * explicit primary keys can be used. Assignment can
 * be handled based on geom_area column or place_guid
 */
CREATE TABLE spatial.place (
  place_guid text PRIMARY KEY,
  name text,
  geom_center geometry(Point, 4326),
  geom_area geometry(Polygon, 4326))
INHERITS (
  social.user_hll, -- e.g. number of users/place
  topical.post_hll, -- e.g. number of posts/place
  temporal.pud_hll, -- e.g. number of userdays/place
  topical.utl_hll, -- e.g. number of terms/place
  spatial.latlng_hll -- e.g. number of locations/place
);


/** City Table
 */
CREATE TABLE spatial.city (
  city_guid text PRIMARY KEY,
  name text,
  geom_center geometry(Point, 4326),
  geom_area geometry(Polygon, 4326))
INHERITS (
  social.user_hll, -- e.g. number of users/city
  topical.post_hll, -- e.g. number of posts/city
  temporal.pud_hll, -- e.g. number of userdays/city
  topical.utl_hll, -- e.g. number of terms/city
  spatial.latlng_hll -- e.g. number of locations/city
);


/** Region Table
 */
CREATE TABLE spatial.region (
  region_guid text PRIMARY KEY,
  name text,
  geom_center geometry(Point, 4326),
  geom_area geometry(Polygon, 4326))
INHERITS (
  social.user_hll, -- e.g. number of users/region
  topical.post_hll, -- e.g. number of posts/region
  temporal.pud_hll, -- e.g. number of userdays/region
  topical.utl_hll, -- e.g. number of terms/region
  spatial.latlng_hll -- e.g. number of locations/region
);


/** Country Table
 */
CREATE TABLE spatial.country (
  country_guid text PRIMARY KEY,
  name text,
  geom_center geometry(Point, 4326),
  geom_area geometry(Polygon, 4326))
INHERITS (
  social.user_hll, -- e.g. number of users/country
  topical.post_hll, -- e.g. number of posts/country
  temporal.pud_hll, -- e.g. number of userdays/country
  topical.utl_hll, -- e.g. number of terms/country
  spatial.latlng_hll -- e.g. number of locations/country
);


/** Temporal Facet
 */
/** timestamp
 * this is the most accurate partition
 * of the temporal facet
 */
CREATE TABLE temporal.timestamp( timestamp timestamp PRIMARY KEY)
INHERITS (
  social.user_hll, -- e.g. number of users/timestamp
  topical.post_hll -- e.g. number of posts/timestamp
);


/** date
 * a unique date reference consisting of year, month and day
 * a unique date can have a name
 */
CREATE TABLE temporal.date( date date PRIMARY KEY, name text)
INHERITS (
  social.user_hll, -- e.g. number of users/day
  topical.post_hll -- e.g. number of posts/day
);


/** month
 * a unique month reference consisting of year and month
 * format: yyyy-mm
 */
CREATE TABLE temporal.month (
  year smallint,
  month smallint,
  PRIMARY KEY (year, month))
INHERITS (
  social.user_hll, -- e.g. number of users/month
  topical.post_hll -- e.g. number of posts/month
);


/** year
 * a unique year reference
 * format: yyyy
 */
CREATE TABLE temporal.year (
  year smallint PRIMARY KEY)
INHERITS (
  social.user_hll, -- e.g. number of users/year
  topical.post_hll -- e.g. number of posts/year
);


/** timeofday
 * e.g. hh-mm-ss
 */
CREATE TABLE temporal.timeofday( time time PRIMARY KEY)
INHERITS (
  social.user_hll, -- e.g. number of users/time of day
  topical.post_hll -- e.g. number of posts/time of day
);


/** hourofday
 * e.g. hh 1-24
 */
CREATE TABLE temporal.hourofday (
  hourofday smallint PRIMARY KEY)
INHERITS (
  social.user_hll, -- e.g. number of users/hour of day
  topical.post_hll -- e.g. number of posts/hour of day
);


/** dayofweek
 * weekday 1 (Monday) - 7 (Sunday)
 */
CREATE TABLE temporal.dayofweek (
  dayofweek smallint PRIMARY KEY,
  name text)
INHERITS (
  social.user_hll, -- e.g. number of users/day of week
  topical.post_hll -- e.g. number of posts/day of week
);


/** dayofmonth
 * day of month, e.g. 1 to (max) 31
 */
CREATE TABLE temporal.dayofmonth (
  dayofmonth smallint PRIMARY KEY)
INHERITS (
  social.user_hll, -- e.g. number of users/day of month
  topical.post_hll -- e.g. number of posts/day of month
);


/** dayofyear
 * day of year, e.g. format mm-dd
 */
CREATE TABLE temporal.dayofyear (
  month smallint,
  day smallint,
  PRIMARY KEY (month, day),
  name text)
INHERITS (
  social.user_hll, -- e.g. number of users/day of year
  topical.post_hll -- e.g. number of posts/day of year
);


/** monthofyear
 * month of year, e.g. format mm
 */
CREATE TABLE temporal.monthofyear (
  monthofyear smallint PRIMARY KEY,
  name text)
INHERITS (
  social.user_hll, -- e.g. number of users/day of year
  topical.post_hll -- e.g. number of posts/day of year
);


/** Social Facet
 * Note: many of the base entities in this facet
 * may be considered as a personal identifier,
 * particularly those of lower granularity (e.g. user, friends),
 * use with caution
 */
CREATE TABLE social.user (
  user_guid text PRIMARY KEY,
  name text)
INHERITS (
  topical.post_hll -- e.g. number of posts/user
);

CREATE TABLE social.friends (
  user_guid_a text,
  user_guid_b text,
  PRIMARY KEY (user_guid_a, user_guid_b),
  direction text)
INHERITS (
  spatial.place_hll, -- e.g. number of distinct places visited together
  topical.term_hll, -- e.g. number of distinct terms used together
  temporal.pud_hll -- e.g. number of distinct userdays of common activity
);

CREATE TABLE social.community (
  id int PRIMARY KEY,
  name text)
INHERITS (
  social.user_hll, -- e.g. number of users part of this community
  topical.post_hll, -- e.g. number of posts of this community
  spatial.place_hll, -- e.g. number of distinct places visited by this community
  spatial.latlng_hll, -- e.g. number of distinct coordinates visited by this community
  topical.term_hll, -- e.g. number of distinct terms used by this community
  temporal.pud_hll -- e.g. number of distinct userdays of activity
);

CREATE TABLE social.culture (
  id text PRIMARY KEY)
INHERITS (
  social.user_hll, -- e.g. number of users part of this culture
  topical.post_hll, -- e.g. number of posts of this culture
  spatial.place_hll, -- e.g. number of distinct places visited by this culture
  topical.term_hll, -- e.g. number of distinct terms used by this culture
  temporal.pud_hll -- e.g. number of distinct userdays of activity
);


/** Topical Facet
 */
CREATE TABLE topical.hashtag (
  hashtag text PRIMARY KEY)
INHERITS (
  social.user_hll, -- e.g. number of users using this tag
  topical.post_hll, -- e.g. number of posts with this tag
  spatial.latlng_hll, -- e.g. number of latlng coordinates where this tag is used
  spatial.place_hll, -- e.g. number of distinct places where this tag is used
  temporal.pud_hll -- e.g. number of distinct userdays where this tag is used
);

CREATE TABLE topical.emoji (
  emoji text PRIMARY KEY)
INHERITS (
  social.user_hll, -- e.g. number of users using this emoji
  topical.post_hll, -- e.g. number of posts with this emoji
  spatial.latlng_hll, -- e.g. number of latlng coordinates where this emoji is used
  spatial.place_hll, -- e.g. number of distinct places where this emoji is used
  temporal.pud_hll -- e.g. number of distinct userdays where this emoji is used
);

CREATE TABLE topical.term (
  term text PRIMARY KEY)
INHERITS (
  social.user_hll, -- e.g. number of users using this term
  topical.post_hll, -- e.g. number of posts with this term
  spatial.latlng_hll, -- e.g. number of latlng coordinates where this term is used
  spatial.place_hll, -- e.g. number of distinct places where this term is used
  temporal.pud_hll -- e.g. number of distinct userdays where this term is used
);

CREATE TABLE topical.topic (
  topic_guid text PRIMARY KEY,
  description text)
INHERITS (
  social.user_hll, -- e.g. number of users active in this topic
  topical.post_hll, -- e.g. number of posts relating to this topic
  topical.term_hll, -- e.g. number of terms relating to this topic
  spatial.latlng_hll, -- e.g. number of latlng coordinates this topic relates to
  spatial.place_hll, -- e.g. number of distinct places this topic relates to
  temporal.pud_hll -- e.g. number of distinct userdays where this topic is used
);

CREATE TABLE topical.domain (
  domain_guid text PRIMARY KEY,
  description text)
INHERITS (
  social.user_hll, -- e.g. number of users active in this domain
  topical.post_hll, -- e.g. number of posts relating to this domain
  topical.term_hll, -- e.g. number of terms relating to this domain
  spatial.latlng_hll, -- e.g. number of latlng coordinates this domain relates to
  spatial.place_hll, -- e.g. number of distinct places this domain relates to
  temporal.pud_hll -- e.g. number of distinct userdays where this domain is used
);


/*
 * 4. Composite Bases
 * ----------------
 * Composite bases combine attributes of different facets
 * These c-bases are typically heavy on computation and memory, since
 * the number of possible combinations increases significantly.
 *
 * We've provided one example of a c-base: the spatial-term base.
 * This base is suitable if you want to apply topic visualization techniques
 * in a spatial context.
 *
 * C-bases are organized in the 'smaller' base component, e.g.: spatial-term
 * base is placed under the topical facet because there can be multiple terms
 * per location. This is a design decision and may change in future releases.
 *
 * C-bases names start with an underscore and
 * both base names are separated with an underscore
 *
 * Privacy: C-bases can be of higher privacy relevance because of the
 * increased number of combinations. A possible approach is to only considered
 * c-base records with a minimum user_count - this will require a two-pass
 * hll generation, because the number of users is initially unknown.
 */
CREATE TABLE topical._term_latlng (
  latitude float,
  longitude float,
  term text,
  PRIMARY KEY (latitude, longitude, term),
  latlng_geom geometry(Point, 4326) NOT NULL)
INHERITS (
  social.user_hll, -- e.g. number of users using this c-base
  topical.post_hll, -- e.g. number of posts with this c-base
  temporal.pud_hll -- e.g. number of distinct userdays where this c-base is used
);

CREATE TABLE topical._hashtag_latlng (
  latitude float,
  longitude float,
  hashtag text,
  PRIMARY KEY (latitude, longitude, hashtag),
  latlng_geom geometry(Point, 4326) NOT NULL)
INHERITS (
  social.user_hll, -- e.g. number of users using this c-base
  topical.post_hll, -- e.g. number of posts with this c-base
  temporal.pud_hll -- e.g. number of distinct userdays where this c-base is used
);

CREATE TABLE topical._emoji_latlng (
  latitude float,
  longitude float,
  emoji text,
  PRIMARY KEY (latitude, longitude, emoji),
  latlng_geom geometry(Point, 4326) NOT NULL)
INHERITS (
  social.user_hll, -- e.g. number of users using this c-base
  topical.post_hll, -- e.g. number of posts with this c-base
  temporal.pud_hll -- e.g. number of distinct userdays where this c-base is used
);

CREATE TABLE temporal._month_latlng (
  year smallint,
  month smallint,
  latitude float,
  longitude float,
  PRIMARY KEY (year, month, latitude, longitude),
  latlng_geom geometry(Point, 4326) NOT NULL)
INHERITS (
  social.user_hll, -- e.g. number of users using this c-base
  topical.post_hll, -- e.g. number of posts with this c-base
  temporal.pud_hll -- e.g. number of distinct dates where this c-base is used
);

CREATE TABLE temporal._month_hashtag (
  month smallint,
  year smallint,
  hashtag text,
  PRIMARY KEY (year, month, hashtag))
INHERITS (
  social.user_hll, -- e.g. number of users using this c-base
  topical.post_hll, -- e.g. number of posts with this c-base
  temporal.pud_hll -- e.g. number of distinct dates where this c-base is used
);

/*
 * 4. Experimental Composite Bases
 * ----------------
 * Below are combinations that are considered experimental.
 * These store data at relatively high resolution, providing little
 * benefits to privacy, particularly for small data sets. These
 * bases will need to be aggregated further, e.g. using HLL union,
 * and based on the individual characteristics (e.g. distribution) of data,
 * such as Quad Trees, topic classification, or temporal patterns (etc.)
 */
CREATE TABLE temporal._month_hashtag_latlng (
  month smallint,
  year smallint,
  hashtag text,
  latitude float,
  longitude float,
  PRIMARY KEY (year, month, hashtag, latitude, longitude),
  latlng_geom geometry(Point, 4326) NOT NULL)
INHERITS (
  social.user_hll, -- e.g. number of users using this c-base
  topical.post_hll, -- e.g. number of posts with this c-base
  temporal.pud_hll -- e.g. number of distinct dates where this c-base is used
);

