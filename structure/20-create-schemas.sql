/* Delete default schemas
 */
DROP SCHEMA IF EXISTS public CASCADE;

/* Delete custom schemas
 */
DROP SCHEMA IF EXISTS social CASCADE;
DROP SCHEMA IF EXISTS spatial CASCADE;
DROP SCHEMA IF EXISTS temporal CASCADE;
DROP SCHEMA IF EXISTS topical CASCADE;

/** Create Schemas
 */
CREATE SCHEMA social;
CREATE SCHEMA spatial;
CREATE SCHEMA temporal;
CREATE SCHEMA topical;

/** Full access rights
 */
GRANT ALL ON SCHEMA social TO postgres, public;
GRANT ALL ON SCHEMA spatial TO postgres, public;
GRANT ALL ON SCHEMA temporal TO postgres, public;
GRANT ALL ON SCHEMA topical TO postgres, public;

/* Add schemas to search path
 */
 ALTER DATABASE :DBNAME SET search_path = "$user", social, spatial, temporal, topical, relations, extensions;
